import * as T from 'three'

import EntityBase from './entity-base'

export class Terrain extends EntityBase {
  load({ scene }) {
    const texture = new T.TextureLoader().load('assets/textures/0.jpg', texture => {
      texture.wrapS = texture.wrapT = T.RepeatWrapping
      texture.offset.set(0, 0)
      texture.repeat.set(16, 16)
    })
    
    const geometry = new T.PlaneGeometry(100, 100, 64, 64)
    const material = new T.MeshPhongMaterial({
      map: texture,
      color: 0xEEEEEE,
      shininess: 10,
      wireframe: false
    })
    
    this.state.mesh = new T.Mesh(geometry, material)
    this.state.mesh.castShadow = false
    this.state.mesh.receiveShadow = true

    scene.add(this.state.mesh)
  }
}

export default Terrain
