import uuid from 'uuid'

export class EntityBase {
  constructor() {
    this.uid = uuid.v4()
    this.state = {}
  }

  load() {}
  update() {}
}

export default EntityBase
