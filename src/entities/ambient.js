import * as T from 'three'

import EntityBase from './entity-base'

const DEFAULT_HEMISPHERE_NEAR_COLOR = 0x5b798a
const DEFAULT_HEMISPHERE_FAR_COLOR = 0x5a744a
const DEFAULT_AMBIENT_COLOR = 0x444444
const DEFAULT_SUN_COLOR = 0xFFFFFF

export class Ambient extends EntityBase {
  load({ scene }) {
    this.state.hemisphere = new T.HemisphereLight(
      DEFAULT_HEMISPHERE_NEAR_COLOR,
      DEFAULT_HEMISPHERE_FAR_COLOR,
      0.35
    )

    this.state.ambient = new T.AmbientLight(DEFAULT_AMBIENT_COLOR, 1)
    this.state.sun = new T.PointLight(DEFAULT_SUN_COLOR, 0.75, 75)

    this.state.sun.castShadow = true
    this.state.sun.shadowDarkness = 0.25
    this.state.sun.shadowMapWidth = 2048
    this.state.sun.shadowMapHeight = 2048
    this.state.sun.position.set(-5, 5, 25)

    scene.add(this.state.hemisphere)
    scene.add(this.state.ambient)
    scene.add(this.state.sun)
  }
}

export default Ambient
