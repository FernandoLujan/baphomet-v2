import * as T from 'three'

import EntityBase from './entity-base'

export class Character extends EntityBase {
  load({ scene }) {
    const geometry = new T.BoxGeometry(1, 1, 1)
    const material = new T.MeshPhongMaterial({ color: 0xCCCCCC })

    this.state.mesh = new T.Mesh(geometry, material)

    this.state.mesh.castShadow = true
    this.state.mesh.receiveShadow = false
    this.state.mesh.position.set(0, 0, 1)

    scene.add(this.state.mesh)
  }

  update() {
    const {mesh} = this.state

    mesh.rotation.x += 0.015
    mesh.rotation.y += 0.015
  }
}

export default Character
