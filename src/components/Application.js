import React, { Component } from 'react';

import GraphicsContext from './GraphicsContext'
import FullscreenContainer from './FullscreenContainer'

import Title from './UI/Title'
import FPSDisplay from './UI/FPSDisplay'
import PlayerPositionDisplay from './UI/PlayerPositionDisplay'

class Application extends Component {
  render() {
    return (
      <div>
        <GraphicsContext />
        <FullscreenContainer>
          <Title />
          <FPSDisplay />
          <PlayerPositionDisplay x={0} y={0} />
        </FullscreenContainer>
      </div>
    )
  }
}

export default Application
