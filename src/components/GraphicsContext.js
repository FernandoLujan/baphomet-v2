import React, { Component } from 'react'
import { connect } from 'react-redux'
import throttle from 'lodash/throttle'

import FullscreenContainer from './FullscreenContainer'
import * as engine from '../engine'
import { setFPS } from '../actions/diagnostics'

const FPS_CAPTURE_INTERVAL = 500

export class GraphicsContext extends Component {
  deltas = []

  constructor(props) {
    super(props)
    this.throttledFPSSetter = throttle(this.props.setFPS, FPS_CAPTURE_INTERVAL)
  }

  componentDidMount() {
    engine.initialize(this.rootElement)
    this.start()
  }

  componentWillUnmount(){
    engine.quit()
    this.stop()
  }

  start() {
    if (!this.frameId) {
      this.frameId = requestAnimationFrame(this.frame.bind(this))
    }
  }

  stop() {
    cancelAnimationFrame(this.frameId)
  }

  frame() {
    this.calculateFPS()
    engine.render()
    this.frameId = window.requestAnimationFrame(this.frame.bind(this))
  }

  calculateFPS() {
    const now = performance.now()

    while (this.deltas.length > 0 && this.deltas[0] <= now - 1000) {
      this.deltas.shift()
    }

    this.deltas.push(now)
    this.throttledFPSSetter(this.deltas.length)
  }

  render() {
    return <FullscreenContainer ref={element => this.rootElement = element} />
  }
}

const mapDispatchToProps = dispatch => ({
  setFPS: fps => dispatch(setFPS(fps))
})

export default connect(null, mapDispatchToProps)(GraphicsContext)
