import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { fonts } from '../../constants/theme'

const Container = styled.div`margin: 0px 30px;`
const Info =      styled.p`
  font-family: ${fonts.normal};
  text-shadow: 2px 2px 1px rgba(0, 0, 0, 0.4);
  font-size: 14px;
  margin: 0;
`

export const PlayerPositionDisplay = ({x, y}) =>
  <Container>
    <Info>
      <strong>Position:</strong> {x}, {y}
    </Info>
  </Container>

PlayerPositionDisplay.propTypes = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired
}

export default PlayerPositionDisplay
