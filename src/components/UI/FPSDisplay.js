import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fonts } from '../../constants/theme'

const Container = styled.div`margin: 25px 30px 0;`
const Info = styled.p`
  font-family: ${fonts.normal};
  text-shadow: 2px 2px 1px rgba(0, 0, 0, 0.4);
  font-size: 14px;
  margin: 0;
`

const FPSDisplay = ({ diagnostics }) =>
  <Container>
    <Info>{diagnostics.fps} <strong>FPS</strong></Info>
  </Container>

FPSDisplay.propTypes = {
  diagnostics: PropTypes.shape({
    fps: PropTypes.number.isRequired
  }).isRequired
}

const mapStateToProps = state => {
  return { diagnostics: state.diagnostics }
}

export default connect(mapStateToProps)(FPSDisplay)
