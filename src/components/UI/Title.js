import React from 'react'
import styled from 'styled-components';

import meta from '../../../package.json'

const Container = styled.div`
  text-shadow: 2px 2px 1px rgba(0, 0, 0, 0.4);
  margin: 25px 30px;
`

const Name =      styled.h2`margin: 0 0 6px;`
const Caption =   styled.p`margin: 0;`
const Version =   styled.span`
  font-size: 12px;
  margin-left: 5px;
`

export const Title = () =>
  <Container>
    <Name>Baphomet</Name>
    <Caption>
      Dark Fables
      <Version>({meta.version})</Version>
    </Caption>
  </Container>

export default Title
