import { combineReducers } from 'redux'
import diagnostics from './diagnostics'
import player from './player'

export default combineReducers({
  diagnostics,
  player
})
