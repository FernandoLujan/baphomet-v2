const initialState = {
  fps: 0
}

const diagnostics = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_FPS':
      return {
        ...state,
        fps: action.fps
      }
    default:
      return state
  }
}

export default diagnostics
