import * as T from 'three'

import {bindEventHandlers, unbindEventHandlers} from './input'
import {Ambient, Character, Terrain} from '../entities'

const DEFAULT_CAMERA_DISTANCE = 15

const renderer = new T.WebGLRenderer({ antialias: true })

export const scene = new T.Scene()
export const raycaster = new T.Raycaster()
export const userMousePosition = new T.Vector2()

const entities = [
  new Ambient(),
  new Terrain(),
  new Character()
]

let rootElement
let camera

/**
 * @name load
 * 
 * @descripton Load and initialize game components and assets.
 */
const load = () => {
  for (let i = 0; i < entities.length; i++) {
    const entity = entities[i];
    entity.load({ scene })
  }
}

/**
 * @name update
 * 
 * @description Process current game state before rendering a frame.
 */
const update = () => {
  for (let i = 0; i < entities.length; i++) {
    const entity = entities[i];
    entity.update()
  }

  raycaster.setFromCamera(userMousePosition, camera)
}

/**
 * @name initialize
 *
 * @param {DOMElement} domRoot 
 */
export const initialize = domRoot => {
  rootElement = domRoot

  const viewportWidth = rootElement.clientWidth
  const viewportHeight = rootElement.clientHeight

  camera = new T.PerspectiveCamera(75, viewportWidth / viewportHeight, 0.1, 1000)
  camera.position.z = DEFAULT_CAMERA_DISTANCE

  rootElement.appendChild(renderer.domElement)
  renderer.setSize(viewportWidth, viewportHeight)
  renderer.shadowMapEnabled = true
  renderer.shadowMapType = T.PCFSoftShadowMap;
  renderer.setClearColor(0x000000)

  load()
  bindEventHandlers()
}

/**
 * @name render
 * 
 * @description Process a single frame.
 */
export const render = () => {
  update()

  renderer.render(scene, camera)
}

/**
 * @name quit
 * 
 * @description Cleanup engine components and DOM.
 */
export const quit = () => {
  unbindEventHandlers()

  rootElement.removeChild(renderer.domElement)
}
