import throttle from 'lodash/throttle'

import { scene, raycaster, userMousePosition } from './engine'

const CAMERA_ZOOM_RANGE = 5
const INPUT_EVENT_UPDATE_INTERVAL = 10

const bind = (eventName, callback) =>
  window.addEventListener(eventName, callback)

const unbind = (eventName, callback) =>
  window.removeEventListener(eventName, callback)

/**
 * @name bindEventHandlers
 */
const bindEventHandlers= () => {
  bind("mousemove", onMouseMove)
  bind("scroll", onMouseScroll)
  bind("wheel", onMouseScroll)
  bind("mousedown", onMouseClick)
  bind("resize", onWindowResize)
}

/**
 * @name unbindEventHandlers
 */
const unbindEventHandlers = () => {
  unbind("mousemove", onMouseMove)
  unbind("scroll", onMouseScroll)
  unbind("wheel", onMouseScroll)
  unbind("mousedown", onMouseClick)
  unbind("resize", onWindowResize)
}

/**
 * @name onMouseMove
 */
const onMouseMove = throttle((event) => {
  userMousePosition.x = (event.clientX / window.innerWidth) * 2 - 1
  userMousePosition.y = -(event.clientY / window.innerHeight) * 2 + 1
}, INPUT_EVENT_UPDATE_INTERVAL)

/**
 * @name onMouseScroll
 */
const onMouseScroll = throttle((event) => {
  // console.log('Mouse scroll...', CAMERA_ZOOM_RANGE)
}, INPUT_EVENT_UPDATE_INTERVAL)

/**
 * @name onMouseClick
 */
const onMouseClick = event => {
  const intersects = raycaster.intersectObjects(scene.children)
  
  const { x, y } = intersects[0].point

  console.log('intersects', intersects)
  console.log(`x: ${x}, y: ${y}`)
}

/**
 * @name onWindowResize
 */
const onWindowResize = throttle((event) => {
  console.log('Window resize...')
}, INPUT_EVENT_UPDATE_INTERVAL)

export {
  bindEventHandlers,
  unbindEventHandlers,
}
